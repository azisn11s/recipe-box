import Vue from 'vue'

import App from './App/vue'

import route from './router'



const app = new Vue({
	el: '#root',
	template: `<app></app>`,
	components: { App },
	router
})

